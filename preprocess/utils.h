//
//  utils.h
//  main
//
//  Created by DAEJIN HYEON on 11/15/14.
//  This is modules for extracting contour
//

#ifndef __main__utils__
#define __main__utils__

#include <iostream>
#include <opencv/highgui.h>
#include <opencv/cv.h>
#include <opencv/cxcore.h>
#include <opencv/ml.h>
#include <iostream>
#include <math.h>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/ml/ml.hpp>
#include <fstream>
#include <sstream>
#include <string>
#include <string.h>   //strtok
#include <locale>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <cstring>

#endif /* defined(__main__utils__) */
typedef struct Node{
    int prev;
    int next;
    int index;
}NODE;
CvSeq* extractContour(IplImage* raw, CvMemStorage* storage, int type);
IplImage* crop( IplImage* src,  CvRect roi);
void getColor(IplImage* src, int x, int y, int* dst);
CvSeq* extractContour(IplImage* raw, CvMemStorage* storage, int type);
IplImage* getResizedImage(IplImage* src, float ratio);
IplImage* featureWatershed(std::string filename, float ratio);
IplImage* featureWatershed(char* filename, float ratio);
IplImage* featureWatershed(IplImage* itempmg, float ratio);
void differenceImage(IplImage* src,IplImage* dst, IplImage* result);
IplImage* featureStep(std::string image_path, CvSeq* contour);
void getRectVerticesFromContour(CvSeq* contour, CvPoint* vertices);
float caculatePointLineDistance(CvPoint line1, CvPoint line2, CvPoint point);
float calculateErrorWithRect(CvPoint* vertices, CvPoint point);
float penaltyFunction(float value, float arg0);
float getEculidianDistance(CvPoint point1, CvPoint point2);
IplImage* getStandardImage(CvPoint* vertices, IplImage* result);
IplImage* getStandardImage(CvPoint* vertices, IplImage* result, int width, int height);
void convertRGBtoHSV(float colors[], float* hsv);
void getRGB(IplImage* src, IplImage* mask, float* hsv);
void getRGB(IplImage* src, float* hsv);
void getMedianRGB(IplImage* src, int* hsv);
void clustering(NODE* node_list, IplImage* gray_image, IplImage* original_image, int** check,int* cluster_bin, int* cluster_size, float thres, NODE* node_array, int* bins, bool plus );
int getClass(int class_idx, int* class_bin);
IplImage* getClusteredImage(IplImage* original, float threshold);
bool getPeak(IplImage* src, int y, int x, int size, float threshold);
void removeLight(IplImage* src, int threshold);
void findColorFromHSV(float* hsv);
